# Contributor: Guy Godfroy <guy.godfroy@gugod.fr>
# Maintainer: Guy Godfroy <guy.godfroy@gugod.fr>
pkgname=gotify-cli
pkgver=2.2.4
pkgrel=1
pkgdesc="A command line interface for pushing messages to gotify"
url="https://gotify.net"
arch="all"
license="MIT"
makedepends="go"
source="$pkgname-$pkgver.tar.gz::https://github.com/gotify/cli/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/cli-$pkgver"
options="net"

build() {
	export LD_FLAGS="-X main.Version=$pkgver -X main.BuildDate=$(date "+%F-%T")"
	go build -ldflags="${LD_FLAGS}" -o gotify cli.go
}

check() {
	go test ./...
}

package() {
	install -m755 -D "$builddir"/gotify "$pkgdir"/usr/bin/gotify
}

sha512sums="
e94e9aeec7479b2c9900b0cfb81df27fc25c692bee100cb669717a46a3505d8c79537b74f5f11a4dc237684bd82af17f2ab6c20a223563122eefe312ccb79968  gotify-cli-2.2.4.tar.gz
"
