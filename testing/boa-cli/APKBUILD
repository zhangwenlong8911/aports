# Contributor: lauren n. liberda <lauren@selfisekai.rocks>
# Maintainer: lauren n. liberda <lauren@selfisekai.rocks>
pkgname=boa-cli
pkgver=0.19
pkgrel=0
pkgdesc="Embeddable and experimental Javascript engine written in Rust"
url="https://github.com/boa-dev/boa/"
# 32-bit: failing tests
# ppc64le: corosensei crate
# s390x: nix crate
# loongarch64: blocked by libc crate
arch="all !armhf !armv7 !x86 !ppc64le !s390x !loongarch64"
license="MIT OR Unlicense"
makedepends="cargo cargo-auditable"
checkdepends="openssl-dev"
source="https://github.com/boa-dev/boa/archive/refs/tags/v$pkgver/boa-$pkgver.tar.gz"
builddir="$srcdir/boa-$pkgver"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --package boa_cli --release --frozen --bin boa
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/boa "$pkgdir"/usr/bin/boa
}

sha512sums="
b996fe8026611a6b8eaad2239b40c0a367a3d18cbe6157f97e722e31faf577c772938c56b17e7efa915bc311f6cbb25a0d2819ffdfc622e1d386e3cc4112a890  boa-0.19.tar.gz
"
