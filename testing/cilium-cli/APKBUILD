# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=cilium-cli
pkgver=0.16.11
pkgrel=0
pkgdesc="CLI to install, manage and troubleshoot Kubernetes clusters running Cilium"
url="https://cilium.io/"
arch="all"
license="Apache-2.0"
makedepends="go"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/cilium/cilium-cli/archive/refs/tags/v$pkgver.tar.gz
	byteorder-add-s390x-support.patch
	byteorder-add-loongarch-support.patch
	"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -v \
		-ldflags "-X github.com/cilium/cilium-cli/internal/cli/cmd.Version=v$pkgver" \
		./cmd/cilium

	for shell in bash fish zsh; do
		./cilium completion $shell > cilium.$shell
	done
}

check() {
	go test ./...
}

package() {
	install -Dm755 cilium -t "$pkgdir"/usr/bin/

	install -Dm644 cilium.bash \
		"$pkgdir"/usr/share/bash-completion/completions/cilium
	install -Dm644 cilium.fish \
		"$pkgdir"/usr/share/fish/vendor_completions.d/cilium.fish
	install -Dm644 cilium.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_cilium
}

sha512sums="
5ce920211563dac9d39baf3bf375d4ff3a0172e175dd88a2c4765d5f981fa5c4f2952a3c725fe806b880b294017394498b5f2ae28e0057522ff803e9f5879f26  cilium-cli-0.16.11.tar.gz
872ccb44ac714420f87c476743d7be1b644c3a5c7e2a975d47720dc970c632ce82bb678421b053ebbc219d4a3b1f2c9f37aa74f3733a055b0f8c1dbe15fed23f  byteorder-add-s390x-support.patch
a0fadbf5b6691eeaed63297fb3a53a35977550169885f1bf283403b453d8d23cb42b7d1b94fd8519c58e5deb8e87b1a21b074d4224881cacd2d74f3c1dd67c2e  byteorder-add-loongarch-support.patch
"
