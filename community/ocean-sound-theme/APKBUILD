# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=ocean-sound-theme
pkgver=6.1.2
pkgrel=0
pkgdesc="Ocean Sound Theme for Plasma"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://invent.kde.org/plasma/ocean-sound-theme"
license="BSD-2-Clause AND CC-BY-SA-4.0 and CC0-1.0"
makedepends="
	extra-cmake-modules
	qt6-qttools-dev
	samurai
	"
_repo_url="https://invent.kde.org/plasma/ocean-sound-theme.git"
source="https://download.kde.org/stable/plasma/$pkgver/ocean-sound-theme-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
035605a78c2f07651b693e6e7b7e923c5b4460652d87b3fd4120f25ea37e7c7bbfb5b956a96fbc789ce7f8cd3ee36241c35d123b92e99e1fd786228f10458362  ocean-sound-theme-6.1.2.tar.xz
"
