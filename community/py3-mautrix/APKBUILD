# Contributor: Bart Ribbers <bribbers@disroot.org>
# Contributor: fossdd <fossdd@pwned.life>
# Maintainer: fossdd <fossdd@pwned.life>
pkgname=py3-mautrix
pkgver=0.20.5
pkgrel=0
pkgdesc="Python 3 asyncio Matrix framework"
url="https://github.com/mautrix/python"
arch="noarch"
license="MPL-2.0"
depends="
	py3-aiohttp
	py3-aiosqlite
	py3-asyncpg
	py3-attrs
	py3-commonmark
	py3-lxml
	py3-olm
	py3-pycryptodome
	py3-ruamel.yaml
	py3-sqlalchemy
	py3-unpaddedbase64
	python3
	"
makedepends="
	py3-gpep517
	py3-installer
	py3-setuptools
	py3-wheel
	"
checkdepends="
	py3-pytest
	py3-pytest-asyncio
	"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/mautrix/python/archive/v$pkgver/mautrix-python-v$pkgver.tar.gz"
builddir="$srcdir/python-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest -v
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
7e7828dce456d81ff0f5dde9d6ef99a025f04c5e1a9ff2309db9c4ccfda1c9b878075b7fecbd33d501821e8f7b47bd30519de0d8f5a84a54536efff4349d5563  py3-mautrix-0.20.5.tar.gz
"
