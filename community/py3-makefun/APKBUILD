# Contributor: Andy Hawkins <andy@gently.org.uk>
# Maintainer: Andy Hawkins <andy@gently.org.uk>
pkgname=py3-makefun
pkgver=1.15.3
pkgrel=0
pkgdesc="Small library to dynamically create python functions"
url="https://github.com/smarie/python-makefun"
arch="noarch"
license="BSD-3-Clause"
depends="python3"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-setuptools_scm
	py3-wheel
	"
checkdepends="
	py3-pytest
	py3-pytest-runner
	"
subpackages="$pkgname-pyc"
source="https://pypi.python.org/packages/source/m/makefun/makefun-$pkgver.tar.gz"
builddir="$srcdir/makefun-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	PYTHONPATH="$PWD/src" pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/makefun-$pkgver*-py2.py3-none-any.whl
}

sha512sums="
6d48d8e1bdd60ab440b31241a957ba60aa5ae6c77a7a4785dd0a3c6cf4cedd5389ff76d7a309d230bdf1db9be577ab85446741febb30dae0be87e9c3a2d003a0  makefun-1.15.3.tar.gz
"
