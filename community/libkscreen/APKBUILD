# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=libkscreen
pkgver=6.1.2
pkgrel=0
pkgdesc="KDE screen management software"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://kde.org/plasma-desktop/"
license="LGPL-2.1-or-later AND GPL-2.0-or-later AND (GPL-2.0-only OR GPL-3.0-only)"
depends_dev="
	kconfig-dev
	kwayland-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	plasma-wayland-protocols
	qt6-qtbase-dev
	qt6-qttools-dev
	samurai
	"
checkdepends="
	dbus
	dbus-x11
	xvfb-run
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang $pkgname-zsh-completion"
_repo_url="https://invent.kde.org/plasma/libkscreen.git"
source="https://download.kde.org/stable/plasma/$pkgver/libkscreen-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DBUILD_WITH_QT6=ON \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	dbus-run-session -- xvfb-run -a ctest --test-dir build --output-on-failure -E "kscreen-test(backendloader|edid|log)"
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	rm -rf "$pkgdir"/usr/lib/systemd
}

sha512sums="
b5519c9b13c987d1e62a71d748fc32ca28d9b8020287cb9db88a73ed2fbf4fb8236d7adfe3dc7d7cc8b05ab4c34b37cf8626a4540e4e720619ca51ca53c3e583  libkscreen-6.1.2.tar.xz
"
