# Maintainer: Steven Guikal <void@fluix.one>
pkgname=aerc
pkgver=0.18.0
pkgrel=1
pkgdesc="email client for your terminal"
url="https://aerc-mail.org"
arch="all"
license="MIT"
depends="cmd:sh less ncurses"
makedepends="go scdoc notmuch-dev"
checkdepends="gpg gpgme"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://git.sr.ht/~rjarry/aerc/archive/$pkgver.tar.gz"

export GOFLAGS="$GOFLAGS -tags=notmuch"
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	unset LDFLAGS # passed as go linker flags and invalid
	make PREFIX=/usr VERSION=$pkgver
}

check() {
	go test ./...
}

package() {
	unset LDFLAGS
	make install PREFIX=/usr DESTDIR="$pkgdir" VERSION=$pkgver
}

sha512sums="
ac45ce6a0e6078e009713745b29f5631fe6177f9d43044b4de97bb011f1f660476cb280ed12ee0eb2cee71e93589ae3c651df869ca01f88b31b7f3abaadcfeb2  aerc-0.18.0.tar.gz
"
