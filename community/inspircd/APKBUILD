# Contributor: Alex Yam <alex@alexyam.com>
# Maintainer: Sadie Powell <sadie@witchery.services>
pkgname=inspircd
pkgver=4.0.1
pkgrel=0
pkgdesc="internet relay chat daemon (ircd)"
url="https://www.inspircd.org/"
pkgusers="inspircd"
pkggroups="inspircd"
arch="all"
license="GPL-2.0-or-later"
makedepends="
	argon2-dev
	gnutls-dev
	libpq-dev
	rapidjson-dev
	mariadb-dev
	openldap-dev
	pcre2-dev
	perl
	re2-dev
	sqlite-dev
	"
subpackages="$pkgname-doc"
options="!check" # no test suite
source="$pkgname-$pkgver.tar.gz::https://github.com/inspircd/inspircd/archive/v$pkgver.tar.gz"

# secfixes:
#   3.10.0-r0:
#     - CVE-2021-33586

build() {
	./configure \
		--enable-extras "argon2 ldap log_json log_syslog mysql pgsql regex_pcre2 regex_posix regex_re2 sqlite3 ssl_gnutls sslrehashsignal"
	./configure \
		--binary-dir /usr/bin \
		--data-dir /var/lib/inspircd/data \
		--example-dir /usr/share/inspircd/examples \
		--module-dir /usr/lib/inspircd/modules \
		--script-dir /usr/lib/inspircd/scripts \
		--disable-auto-extras \
		--distribution-label alpine$pkgrel \
		--gid inspircd \
		--uid inspircd \
		--system
	INSPIRCD_DISABLE_RPATH=1 make
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
98d31ccdadb7d569b349fcb32a6f339e423d966c39fc1399179a0ed0913fac51ffc98be7600cde60a588ab49f2164c477081e3711c50ccef3b126f92c568ae48  inspircd-4.0.1.tar.gz
"
