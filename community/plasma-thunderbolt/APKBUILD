# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=plasma-thunderbolt
pkgver=6.1.2
pkgrel=0
# armhf blocked by qt6-qtdeclarative
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
pkgdesc="Plasma integration for controlling Thunderbolt devices"
license="GPL-2.0-only OR GPL-3.0-only"
depends="bolt"
makedepends="
	extra-cmake-modules
	kcmutils-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdeclarative-dev
	ki18n-dev
	knotifications-dev
	qt6-qtbase-dev
	qt6-qtdeclarative-dev
	samurai
	"
checkdepends="
	dbus
	xvfb-run
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/plasma/plasma-thunderbolt.git"
source="https://download.kde.org/stable/plasma/$pkgver/plasma-thunderbolt-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	dbus-run-session -- xvfb-run -a ctest --test-dir build --output-on-failure -E "(libkbolt-device|kbolt-kded-kded)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
2176787870c3dd9d4dd6904befb685afaac285679d94f22e3f8d55f8fd4e3d5b6a9f8745c3b434528b1c2c851bc11fb7a5c0ecae693124ad57251e71b9cd8f48  plasma-thunderbolt-6.1.2.tar.xz
"
