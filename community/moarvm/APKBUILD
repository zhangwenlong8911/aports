# Contributor: Curt Tilmes <Curt.Tilmes@nasa.gov>
# Maintainer: Curt Tilmes <Curt.Tilmes@nasa.gov>
pkgname=moarvm
pkgver=2024.06
pkgrel=0
pkgdesc="VM with adaptive optimization and JIT compilation, built for Rakudo"
url="https://moarvm.org/"
arch="all"
license="Artistic-2.0"
depends_dev="$pkgname=$pkgver-r$pkgrel perl zstd-dev libffi-dev libuv-dev"
makedepends="$depends_dev"
subpackages="$pkgname-dev $pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/MoarVM/MoarVM/releases/download/$pkgver/MoarVM-$pkgver.tar.gz"
builddir="$srcdir/MoarVM-$pkgver"
options="!check" # No tests

build() {
	unset CPPFLAGS CFLAGS
	perl Configure.pl --prefix=/usr --has-libffi \
		--has-libuv --no-mimalloc --debug
	make -j"$JOBS"
}

package() {
	make PKGCONFIGDIR=/usr/lib/pkgconfig \
		DESTDIR="$pkgdir" install

	install -Dvm644 Artistic2.txt CREDITS LICENSE \
		MANIFEST README.markdown VERSION \
		-t "$pkgdir"/usr/share/doc/"$pkgname"
	cp -vr docs "$pkgdir"/usr/share/doc/"$pkgname"/
}

dev() {
	default_dev
	amove usr/share/nqp/lib
}

sha512sums="
97d3524e7ea7b7e37835df57d8718dce9d948db902fbd4115175296eb12178d60bf5688300501c23e133ebb9bdc43939f1223c882e92c701ae7c2b3bcea80ba1  moarvm-2024.06.tar.gz
"
