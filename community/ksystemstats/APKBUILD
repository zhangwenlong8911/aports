# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=ksystemstats
pkgver=6.1.2
pkgrel=0
pkgdesc="A plugin based system monitoring daemon"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="LicenseRef-KDE-Accepted-GPL AND LicenseRef-KDE-Accepted-LGPL AND CC0-1.0"
makedepends="
	eudev-dev
	extra-cmake-modules
	kcoreaddons-dev
	kdbusaddons-dev
	kio-dev
	libksysguard-dev
	libnl3-dev
	lm-sensors-dev
	networkmanager-qt-dev
	qt6-qtbase-dev
	samurai
	solid-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/plasma/ksystemstats.git"
source="https://download.kde.org/stable/plasma/$pkgver/ksystemstats-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure -E "ksystemstatstest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	# We don't ship systemd
	rm -r "$pkgdir"/usr/lib/systemd
}

sha512sums="
c75e26dc185903d06ade07010d9463faf7c041178f3d47474ab266a34ae5f3e15baf6cc55a5dd08232e7193bc9277376955b60ec39b0b97b5486cedebdccbdc0  ksystemstats-6.1.2.tar.xz
"
