# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=oxygen-sounds
pkgver=6.1.2
pkgrel=0
pkgdesc="The Oxygen Sound Theme"
# armhf blocked by extra-cmake-modules
arch="noarch !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	qt6-qtbase-dev
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/oxygen-sounds.git"
source="https://download.kde.org/stable/plasma/$pkgver/oxygen-sounds-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
c96b32440f4c1327e4cf200be2f856d33270060ee6939bd33073313a6b943d4bb9de9b3b5a170df29197c721bde9e5bc4f037d7c89bc664d36b7de2ba164ce7b  oxygen-sounds-6.1.2.tar.xz
"
