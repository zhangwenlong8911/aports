# Contributor: Francesco Camuffo <dev@fmac.xyz>
# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=ugrep
pkgver=6.2.0
pkgrel=0
pkgdesc="Ultra fast grep with interactive query UI and fuzzy search"
url="https://ugrep.com/"
arch="all"
license="BSD-3-Clause"
checkdepends="bash"
makedepends="
	brotli-dev
	bzip2-dev
	bzip3-dev
	linux-headers
	lz4-dev
	pcre2-dev
	xz-dev
	zlib-dev
	zstd-dev
	"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/Genivia/ugrep/archive/refs/tags/v$pkgver.tar.gz"

build() {
	CXXFLAGS="$CXXFLAGS -O2 -flto=auto" \
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--with-bzip3
	make
}

check() {
	make test
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
6108acd2b49a7a00816fbd030d809c27b86dbd9c9cdefbc5682893a6908ae538e0df1599a29b2f4fe58f21d305230e317a1a33255e36e2d1eb36e96c58723174  ugrep-6.2.0.tar.gz
"
